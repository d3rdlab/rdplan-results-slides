## Innovation Lab

### Roadmap 2018-2019 Planning

#### Results & Open-Discussion

---

## Possible R&D Scenarios

- Next generation overlays
- Monetization
- Content Discovery
- Social Features

+++

## Next generation overlays (1)

- 3D data viz / augmented reality in video
- 2D/3D stats overlay on live tracked objects/players
  - trajectories, names, distance, tactics, heatmaps, player positions, ball, etc.
- Interactive TV-like overlays
  - cloud or client-side rendering
  - **(something OnStage is exploring?)**

+++

## Next generation overlays (2)

- Push-mode based on user profile, rules, settings, sport type, game events, timing  @fa[arrow-right] a sort of automatic graphics director
- Commentary (on-demand/live) transcription / automatic translation
- Subtitles / artificial voice for impaired users
- Vocal Assitants Support (Google Home / Amazon Echo)

+++

## Monetization

- Immersive ads / augmented reality ads
- Contextual Advertisements/Product placement in video feed (i.e. AR objects placed in the game area, around a player)
- Within the video experience, offer products like jerseys, tickets, or partners products to a captive audience in a non-intrusive way
- Offer relevant products regarding the actions (scorer's jersey, tickets, partners products...)

+++

## Content Discovery (1)

- Automatic video indexing 
  - on new & old video clips, useful to enrich tagged and untagged videos
- Automated video highlights
  - pick up "key moments" in the match by deriving data from players, fans, stats, audio from feed

+++

## Content Discovery (2)

- Search & recommendation engine
  - Video content (actions, players, sport events, scoring, etc.)
  - User profile, friend suggestions, trends, etc.
  - Binge watching, watch history, favorites, pause/resume across devices
  - Per-user customization
- Automatic narrative @fa[arrow-right] data to stories
  - automatic generated for editorial

+++

## Social Features

- (Semi/Full) Automatic video clips generation for sharing on social networks (with graphics/audio customization)

---

## Survey Results

+++

**9 people** answered (on about 50 invited)

+++

**TOP-3** scenarios:

1. Content discovery - Automated video highlights (7/9)
    - pick up "key moments" in the match by deriving data from players, fans, stats, audio from feed
2. Monetization - Brand/player/object/actions identification (4/9)
    - within the video experience, offer products like jerseys, tickets, or partners products to a captive audience in a non-intrusive way, offer relevant products regarding the actions (scorer's jersey, tickets, partners products...)
3. Next gen overlays - Per-user overlay customization (3/9)
    - Push-mode based on user profile, rules, settings, sport type, game events, timing
    @fa[arrow-right] *a sort of automatic per-user graphics director*

+++

**TOP-3** activities:

1. Person/object tracking, recognition & matching (6/9)
    - Find where/which logo/brands/face/player/object is in a picture and/or video.
2. Data Analysis, end-user behavior and pattern recognition (6/9)
    - Access/dump/extract/combine data (possibly a lot) on users, events, sport games, statistics, tracking
3. Automatic Video Analysis (3/9)
    - Extract insights, content, tags, annotations, text OCR, audio transcriptions and other useful metadata from video.

---

## OnStage

#### Broadcast / Digital Convergence vision

+++

- cloud GFX Engine/Studio tools
- remoting GFX/data logging operators
- **automatic data logging technologies**
  - [Signality](https://www.signality.com/) / [SPORTLOGiQ](http://sportlogiq.com/en/)
  - Custom build? [DL R&D](https://modelzoo.co/blog/deep-learning-models-and-code-for-pose-estimation), [Real-time](https://www.youtube.com/watch?v=4PSm8zFE1Cc)
- microservices and orchestration
- **per-user GFX customization**
  - interactive TV graphics (NDI, mask metadata)
  - server-side cloud / client-side rendering (Lottie)
  - AR overlays
  - **AI, recommendation engine, analytics**

For details, slide deck from **Davide Cortassa**

+++

![Convergence Ecosystem](/assets/images/Ecosystem.png)

+++

![Tracking](/assets/videos/Tracking.mp4)

+++

![State-Of-Art DL](/assets/videos/PPN.mp4)

+++

![Interactive TV GFX Demo](/assets/videos/Interactive-TV-GFX.mp4)

+++

![Interactive AR GFX Demo](/assets/videos/AR-graphics.mp4)

+++

![Lottie GFX](/assets/videos/Lottie.mp4)

---

## Diva

+++

- Video list recommendation
  - **AI, recommendation engine, analytics**
- Next gen overlays, in-player monetization & e-games
  - [Genvid Technologies](https://www.genvidtech.com/)
    - The first broadcasting solution built especially for games
  - [Maestro](https://info.maestro.io/)
    - A white label platform for enterprise live streamers to own, engage, and monetize their audiences
- Next gen platforms
  - AR/MR/VR (Mobile devices, HoloLens, Magic Leap)
  - Voice Assistants (Google/Alexa/Cortana, etc.)

---

## Forge

+++

- Smart Editor
  - Automatic content tagging
  - Entities recognition
  - Internal/external content reference matching
  - Images/video references
  - Dashboard and data analysis on content (to suggest editors to write better)
    - something similar to https://textio.com/products/

- New search engine
  - *Automatic content and media tagging would be helpful*

---

### NFL DB100

- AI platform
  - Voice enabled (cross-platform Google/Alexa/etc.)
  - 100-years data archive
  - Recommendation engine
  - Search engine
  - TTS-STT, NLP, Bot, etc.
  - **Forge as CMS**, for text/media content **and voice-related stuff**

+++

![Components](/assets/images/NFL_DB100.png)

---

### Possible R&D activities

+++

- Automatic Image/Video Analysis
  - Extract insights, content, tags, annotations, text OCR, audio transcriptions and other useful metadata from video
  - Person/object tracking, recognition & matching; AR/MR/VR use case?
  - Find where/which logo/brands/face/player/object is in a picture and/or video
- Data Analysis, end-user behavior and pattern recognition
  - Access/dump/extract/combine data (possibly a lot) on users, events, sport games, statistics, tracking
- NLP tools & services (Text analysis, Voice Assistants, Bots, etc.)

<p class="text-center">
  @fa[arrow-down]
</p>

+++

<p class="text-center">
Tools/services PoC for video/image analysis, recommendation and search engine
</p>

- Automatic video indexing, highlights
- Diva per-user overlays and/or custom GFX
- Diva next-gen platforms UI/UX (AR/MR/VR + AI/Voice)
- Diva video list built-in recommendation engine/service
- Forge smart editor & new search engine components and helpers
- Automatic data logging/tracking
- NFL DB100 ICE components and Forge integration

---

### Thank you